def test_generate_pattern_graph():
    from api.graph.pattern_matching import generate_pattern_graph

    patterns = ["AACTGG","AAACTTT"]

    graph, allowed_loops = generate_pattern_graph(patterns)

    print allowed_loops

    assert allowed_loops['A'] == 2
    assert allowed_loops['T'] == 2
    assert allowed_loops['C'] == 0
    assert allowed_loops['G'] == 1

    for node in graph:
        print node,graph[node]

if __name__ == '__main__':
    test_generate_pattern_graph()
    print "Tests passed"
