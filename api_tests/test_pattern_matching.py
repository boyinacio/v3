if __name__ == '__main__':
   from api.graph.pattern_matching import generate_pattern_graph, pattern_matching_in

   graph_patterns = ["AAAG","AAAT","TTTTTCCC","TTTTTG"]

   graph, max_allowed_loops = generate_pattern_graph(graph_patterns)

   for e in graph:
       print e, graph[e]

   print max_allowed_loops

   matching = pattern_matching_in(graph, max_allowed_loops)

   sequence = "GTAATTCGTACT"
   print matching(sequence)

   sequence = "CATTCTTTGAAAATC"
   print matching(sequence)

   sequence = "CGACTACGCGCGCGC"
   print matching(sequence)

   sequence = "ATCAAATTTTTCCGGGATCATC"
   print matching(sequence)
