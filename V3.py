#!env/bin/python

from api.utils import args_has_at_least_length
from api.V3.scripts.create import *
from api.V3.scripts.execute import *

def help_message():
	content = "USAGE: ./V3.py <command> <params>" + "\n"
	content += "Available commands:" + "\n"
	content += "\tcreatemission <mission_name>\tCreates a mission called <mission_name>" + "\n"
	content += "\tcreateexperiment <exp_name>\tCreates an experiment called <exp_name>" + "\n"
	content += "\trunexperiment <exp_name>\tRuns an experiment with name <exp_name>" + "\n"

	return content

def args_processer(scripts_list,flags):
	def process_args(args):
		script_to_run = args[flags['script_to_run']] 
		list_of_scripts = scripts_list.keys()

		if script_to_run in list_of_scripts: 
			return scripts_list[script_to_run](args,flags)
		else:
			return help_message()

	return process_args

def generate_scripts_list():
	script_list = {
		'createmission' : create_mission,
		'createexperiment' : create_experiment,
		'runexperiment' : run_experiment,
	}
	return script_list

def generate_flags():
	flags = {
		'script_to_run' : 0,
		'project_name' : 1,
	}
	return flags

if __name__ == '__main__':
	from sys import argv

	scripts_list = generate_scripts_list() 
	flags = generate_flags()
	
	if not args_has_at_least_length(2)(argv):
		print help_message()
	else:
		v3_processer = args_processer(scripts_list,flags)
		args = argv[1:]
		result = v3_processer(args)
		print result
