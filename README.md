# V3

A python command-line project manager.

# Author

Inácio Medeiros

E-mail: inaciogmedeiros@gmail.com

# Motivation

Are you feeling that your (python, and you can extend to any other language) programs in your directory are becoming a mess, mixing many projects?

Before becoming a PhD student, I've thought about this scenario, and attempting to minimize the mess that buches of programs can make, designed this
tool for "minimizing the mess". Its name is V3.

V3 comes with a command-line app for you creating *missions* and *experiments* (explained below), and an api which contains skeleton for make
V3 working, and some programs that I've developing for "automating boring stuff" in research I'm developing.

# Core Concepts

## Missions

Missions are that projects in which you wanna discovery somethings, but does not have a well-defined process to do it, something like
an "exploratory research".

A mission is composed of a directory which contains a text file, where you must put some description of your mission, and a symbolic link
to V3 api directory.

To create a mission, go to V3 main directory and type

```bash
$ ./V3.py createmission your_mission_name_goes_here
```

## Experiments

Experiments are like missions, but now you've got a well defined process to conduct it.

An experiment is composed of a directory which contains:

+ A description text file;
+ A python script file (in which you must implement your experiment);
+ A symbolic link for V3 api;
+ An \_\_init\_\_.py;
+ A `materials` directory, for storing input files, config files, among any other things you wanna to;
+ A `results` directory, for storing result files from your experiment.

For creating an experiment, go to V3 main directory type

```bash
$ ./V3.py createexperiment your_experiment_name_goes_here
```

### Automating Experiment execution

One thing that V3 does is automating execution of experiments, treating an experiment as a python module,
and running its "run\_experiment" function.

For running an experiment, go to V3 main directory and type

```bash
$ ./V3.py runexperiment your_experiment_name_goes_here
```

# Requirements

+ Unix-like Operating System (Preferentially Linux, because it is being developed in it, and I encourage you to test in other unix-like systems)
+ Python 2.7 or higher (but inside "Python 2.x branch") plus virtualenv

# Installation Instructions

For installing and using V3, follow these steps:

1. Clone this project
2. Create a directory called "missions"
3. Create a directory called "experiments"
..3.1. Inside it, create an file called \_\_init\_\_.py
4. Create an environment with virtualenv (`virtualenv env`)
5. Install requirements (`pip install -r requirements.txt`)

# Using instructions

Everytime you're gonna use V3, first activate your environment (`source env/bin/activate`).
