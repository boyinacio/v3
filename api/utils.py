# utils.py
# General Utilities

'''
name:  executable_runner_function
descr: builds a function which will run an executable with (optionally) a list of params and values
input: path_to_executable (string)
output: a function which runs an  executable with (optionally) a list of params and values (must be a dict whose keys and values can be converted to string)
'''
def executable_runner_function(path_to_executable):
	'''
	name: run_executable
	descr: runs executable with (optionally) a list of params and values (must be a dict whose keys and values can be converted to string)
	input: param_value_list (dictionary)
	'''
	def run_executable(param_value_list=None):
		from os import system
		program = path_to_executable
		if param_value_list is not None:
			program = path_to_executable + " " + reduce(lambda acc, element : acc + " " + str(element) + " " + str(param_value_list[element]), param_value_list)
		system(program)
	return run_executable

'''
name: args_has_at_least_length
descr: given a 'length', returns a function that verifies if a list or arguments has at least 'length' arguments
input: length (int)
output: has_at_least_length(args)
'''
def args_has_at_least_length(length):
	def has_at_least_length(args):
		return len(args) >= length

	return has_at_least_length


'''
name: remove_dots_and_bars
descr: remove dots and bars chars from a string
'''
def remove_dots_and_bars(string):
    return string.replace(".","_").replace("/","_")


'''
name: remove_extension
descr: remove extension from a filename
'''
def remove_extension(string):
    splitted_string = string.split(".")
    return "".join(splitted_string[:-1])


'''
name: get_extension
descr: gets extension from a filename
'''
def get_extension(string):
    return string.split(".")[-1]

def command(command_):
    def execute():
        from os import system
        system(command_)
    return execute
