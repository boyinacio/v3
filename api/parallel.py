# parallel.py
# module for parallelizing jobs

def number_of_workers():
	from multiprocessing import cpu_count
	return cpu_count() / 2

def run_in_parallel(job,dataset):
	from multiprocessing import Pool
	results = None
	pool = Pool(number_of_workers())
	results = pool.map(job,dataset)
	return results
