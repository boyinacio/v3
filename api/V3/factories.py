from models import *

# Factories
# =========================================================
class ProjectFactory(object):
	@staticmethod
	def create_project(name,type_):
		from api.unix import mkdir, touch, ln

		project = ProjectFactory.generate_project(name,type_)

		if project == None:
			return "ERROR: project type wrongly specified."

		project.create_basic_structure()
		project.create_internal_structure()

		return ProjectFactory.successful_created_project_message(project)

	@staticmethod
	def generate_project(name,type_):
		if type_ == "mission": 
			return Mission(name)
		elif type_ == "experiment":
			return Experiment(name)
		else:
			return None

	@staticmethod
	def successful_created_project_message(project):
		if isinstance(project,Mission):
			return "Mission created. Check if a directory called " + project.name + " were created at missions directory, and there is a txt description file in it."
		elif isinstance(project,Experiment):
			return "Experiment " + project.name + " created.\nCheck if a directory for it was created, and inside it, if a description text file and a python script file were created.\nTo run your experiment, type \"$ ./V3.py runexperiment <your_experiment_name>\""
		else:
			return "Project created with success! Verify if there is a directory for it."

# =========================================================
