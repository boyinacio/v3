def run_experiment_name_error_message():
	return "ERROR: You didn't provided experiment's name!\nUsage: $ ./V3.py runexperiment <experiment_name>"

def experiment_exist(name):
	from api.files import directories_from_directory
	from api.V3.models import Project
	experiments_list = directories_from_directory(Project.experiments_directory())
	return name in experiments_list

def exec_experiment(name):
    from importlib import import_module

    if not experiment_exist(name):
        return "ERROR: " + name + " isn't is your experiment list"

    experiment_module = import_module("experiments." + name + ".experiment")

    result = experiment_module.run_experiment()

    return result

def run_experiment(args,flags):
    from api.utils import args_has_at_least_length
    if not args_has_at_least_length(2)(args):
        return run_experiment_name_error_message()

    experiment_name = args[flags['project_name']]

    return exec_experiment(experiment_name)
