def name_error_message(project_type):
	return "ERROR: You didn't provided a name for your "+project_type+"!\nUsage: $ ./V3.py create"+project_type+" <your_"+project_type+"_name>"

def create_project(args,flags,project_type):
	from api.V3.factories import ProjectFactory
	from api.utils import args_has_at_least_length

	if not args_has_at_least_length(2)(args):
		return name_error_message(project_type)
		
	name = args[flags['project_name']]
	type_ = project_type

	return ProjectFactory.create_project(name,type_)

def create_mission(args,flags): return create_project(args,flags,"mission")
def create_experiment(args,flags): return create_project(args,flags,"experiment")
