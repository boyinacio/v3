class Project(object):
	def __init__(self, name):
		self._name = name
		self._dir_prefix = ""

	@property
	def name(self):
		return self._name

	@property
	def directory(self):
		return self._dir_prefix + self._name

	def description_file(self):
		return ""

	def create_basic_structure(self):
		from api.unix import ln, mkdir, pwd
		api_directory = pwd()+"/api"
		create_symbolic_link = True

		mkdir(self.directory)
		ln(api_directory,self.directory+"/api",create_symbolic_link)

	def create_internal_structure(self):
		pass

	@staticmethod
	def missions_directory():
		return "missions/"

	@staticmethod
	def experiments_directory():
		return "experiments/"
	

class Mission(Project):
	def __init__(self, name):
		Project.__init__(self, name)
		self._dir_prefix = Project.missions_directory()

	def create_internal_structure(self):
		from api.files import write_on_file
		filename = self.directory+"/mission_description.txt"
		write_on_file(filename,self.description_file())

	def description_file(self):
		content = "MISSION " + self.name + "\n"
		content += "Author: <put your name here>" + "\n"
		content += "Objectives: " + "\n"
		content += "> <put your objectives here> (once they're completed, make some mark to inform this)" + "\n"

		return content

class Experiment(Project):
	def __init__(self, name):
		Project.__init__(self, name)
		self._dir_prefix = Project.experiments_directory()

	def create_internal_structure(self):
		from api.files import write_on_file
	
		self.create_experiment_directories()		
		self.create_init_file()		
		
		description_filename = self.directory+"/experiment_description.txt" 
		script_filename = self.directory+"/experiment.py"

		write_on_file(description_filename,self.description_file())
		write_on_file(script_filename,self.script_file())

	def description_file(self):
		content = "EXPERIMENT " + self.name + "\n"
		content += "Author: <put your name here>" + "\n"
		content += "Description:" + "\n"
		content += "<put the description of your experiment here, detailing as much as you want to>" + "\n"

		return content

	def script_file(self):
		content = "def run_experiment():" + "\n"
		content += "\treturn \"Implement your experiment in this function. DO NOT change the name of this function\"" + "\n\n"
		content += "if __name__ == '__main__':" + "\n"
		content += "\tresult = run_experiment()" + "\n"
		content += "\tprint result"

		return content

	def create_experiment_directories(self):
		from api.unix import mkdir
		mkdir(self.directory+"/materials")
		mkdir(self.directory+"/results")

	def create_init_file(self):
		from api.unix import mkdir, touch
		touch(self.directory+"/__init__.py")

# =========================================================
