def _psipred_dir():
    return "/data/home/inacio/V3/api/Bio/PSIPRED"

def _run_psipred(sequence_file):
    from api.utils import executable_runner_function
    command = _psipred_dir()+"/tcsh "+_psipred_dir()+"/runpsipred " + sequence_file
    runner = executable_runner_function(command)
    runner()
    return sequence_file.split(".")[0].replace("/","_")

def psipred(sequence_file):
    from api.unix import cd, pwd, cp, rm

    actual_dir = pwd()

    result = _run_psipred(sequence_file)

    rm("*.ss")
    rm("*.horiz")

    return result
