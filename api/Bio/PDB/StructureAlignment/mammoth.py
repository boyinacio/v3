from .StructureAligner import StructureAligner

'''Class for implement wrappers for structure aligners'''
class Mammoth(StructureAligner):
    def __init__(self):
        pass

    @property
    def output_header(self):
        return 'protein_a,protein_b,pss3d,rmsd,evalue,zscore,tm_score'

    def align(pdb_a, pdb_b):
        mammoth_command_path = '/home/inacio/softwares/structure_alignment/mammoth/'
        mammoth_command_executable = 'mammoth'

        output_filename = pdb_a + '_' + pdb_b + '_alignment'

        mammoth_command = mammoth_command_path + mammoth_command_executable
		mammoth_command += ' -p ' + pdb_a
		mammoth_command += ' -e ' + pdb_b
		mammoth_command += ' -o ' + output_filename

        run_mammoth = command(mammoth_command)
        run_mammoth()

        return output_filename

    def parse_output_from_alignment(alignment):
        from api.files import get_lines_of_file

		'''
		lines in alignment file to extract data
		'''
		PREDICTED_PROTEIN_LINE = 20 # passed on test
		PREDICTED_PROTEIN_FIELD = 1 # passed on test

		EXPERIMENTAL_PROTEIN_LINE = 25 # passed on test
		EXPERIMENTAL_PROTEIN_FIELD = 1 # passed on test

		INDEXES_LINE = 34 # passed on test
		PSS3D_FIELD = 2 # passed on test
		RMSD_FIELD = 8 # passed on test

		E_VALUE_LINE = 37 # passed on test
		E_VALUE_FIELD = 1 # passed on test

		Z_SCORE_LINE = 39 # passed on test
		Z_SCORE_FIELD = 1 # passed on test

		TM_SCORE_LINE = 40 # passed on test
		TM_SCORE_FIELD = 1 # passed on test

		alignment_file_lines = get_lines_of_file(alignment)

		if len(alignment_file_lines) <= 13:
			return None

		predicted_protein = alignment_file_lines[PREDICTED_PROTEIN_LINE]
		predicted_protein = predicted_protein.split()[PREDICTED_PROTEIN_FIELD]

		experimental_protein = alignment_file_lines[EXPERIMENTAL_PROTEIN_LINE]
		experimental_protein = experimental_protein.split()[EXPERIMENTAL_PROTEIN_FIELD]

		indexes = alignment_file_lines[INDEXES_LINE]
		pss3d = indexes.split()[PSS3D_FIELD]
		rmsd = indexes.split()[RMSD_FIELD]

		evalue = alignment_file_lines[E_VALUE_LINE]
		evalue = evalue.split()[E_VALUE_FIELD]

		zscore = alignment_file_lines[Z_SCORE_LINE]
		zscore = zscore.split()[Z_SCORE_FIELD]

        tm_score = alignment_file_lines[TM_SCORE_LINE]
		tm_score = tm_score.split()[TM_SCORE_FIELD]

        output = predicted_protein + ',' experimental_protein + ',' + pss3d + ',' + rmsd + ',' + evalue + ',' + zscore + ',' + tm_score

		return output
