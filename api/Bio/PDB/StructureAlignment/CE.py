from .StructureAligner import StructureAligner

'''Class for implement wrappers for structure aligners'''

class CEAligner(StructureAligner):
	def __init__(self):
		self.__file1 = ''
		self.__file2 = ''
		self.__output_format = '-print'
		self.__nrcpu = 1
		self.__output_filename = ''

	@property
	def file1(self):
		return self.__file1

	@property
	def file2(self):
		return self.__file2

	@property
	def output_filename(self):
        if self.__outputfilename == '':
            return self.file1 + '_' + self.file2 + '_alignment'
        else:
            return self.__outputfilename

	@property
	def output_format(self):
		return self.__output_format

	@property
	def nrcpu(self):
		return self.__nrcpu

    @property
    def output_header(self):
        return 'name1,name2,alnLength,gapLen,chainRmsd,identity,similarity,similarity1,similarity2,probability'

	@file1.setter
	def file1(self,filename):
		self.__file1 = filename

	@file2.setter
	def file2(self,filename):
		self.__file2 = filename

	@nrcpu.setter
	def nrcpu(self,nrcpu):
		self.__nrcpu = nrcpu

	@output_filename.setter
	def file2(self,out_filename):
		self.__output_filename = out_filename

	def output_format_xml(self):
		self.__output_format = '-printXML'

	def output_format_ce(self):
		self.__output_format = '-printCE'

	def number_of_cpus(number):
		self.__nrcpu = number

	def align(self, pdb_a=self.file1, pdb_b=self.file2):
        from api.utils import command

        if pdb_a != self.file1: self.file1 = pdb_a
        if pdb_b != self.file2: self.file2 = pdb_b

        ce_command_path = '/home/inacio/old_dir/softwares/structure_alignment/biojava_protein_comparison_tool/'
        ce_command_executable = 'runCE.sh'

        ce_command = ce_command_path + ce_command_executable
		ce_command += ' -file1 ' + self.file1
		ce_command += ' -file2 ' + self.file2
		ce_command += ' -nrCPU ' + str(self.nrcpu)
		ce_command += self.output_format
		ce_command += ' > ' + self.output_filename

        run_ce = command(ce_command)
        run_ce()

        return self.output_filename

    def parse_output_from_alignment(alignment):
        import xmltodict as xml
        from api.files import get_lines_of_file

        alignment_file_string = get_lines_of_file(alignment)

        xml_dict = xml.parse(xml_file)
        afp_chain = xml_dict['AFPChain']

        output = afp_chain['@name1']+','+afp_chain['@name2']+','+afp_chain['@alnLength']+','+afp_chain['@gapLen']+','+afp_chain['@chainRmsd'].replace(',','.')+','+afp_chain['@identity'].replace(',','.')+','+afp_chain['@similarity'].replace(',','.')+','+afp_chain['@similarity1'].replace(',','.')+','+afp_chain['@similarity2'].replace(',','.')+','+afp_chain['@probability'].replace(',','.')

        return output
