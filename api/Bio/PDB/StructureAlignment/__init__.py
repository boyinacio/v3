def alignment_pipeline(pdb_a, pdb_b):
    def align_using(aligner):
        from api.pipeline import pipeline
        pipeline_ = pipeline(aligner.align, aligner.parse_output_from_alignment)
        return pipeline_(pdb_a, pdb_b)

    return align
