def run_tests():
    from SS2 import SS2
    ss2 = SS2("example.ss2")
    print ss2['aa_sequence']
    print ss2['structure_sequence']

    for annotation in sorted(ss2['letter_annotations']):
        print annotation, ss2['letter_annotations'][annotation]

if __name__ == '__main__':
    run_tests()
