# unix.py
# unix commands as functions

from .utils import executable_runner_function

'''
name: mkdir
descr: executes mkdir unix command, for creating directories
input : directory_name string
'''
def mkdir(directory_name):
	create_directory = executable_runner_function("mkdir")
	create_directory({directory_name : ""})


'''
name: cd
descr: executes cd unix command, for accessing directories
input : directory_name string
'''
def cd(directory_name):
    cd_runner = executable_runner_function("cd")
    cd_runner({directory_name : ""})


'''
name: cp
descr: executes cp unix command
'''
def cp(origin,destiny,options=None):
    cp_runner = executable_runner_function("cp")

    if options is None:
        options = ""

    options += " " + origin + " " + destiny

    cp_runner({options : ""})


'''
name: rm
descr: executes rm unix command
'''
def rm(target,options=None):
    rm_runner = executable_runner_function("rm")

    if options is None:
        options = ""

    options += " " + target

    rm_runner({options : ""})

'''
name:  chmod
descr: executes chmod unix command, for changing directory/file permissions
input : options string
        destination string
'''
def chmod(options,destination):
	chmod_run = executable_runner_function("chmod")

	opts = options + " " + destination

	chmod_run({opts : ""})

'''
name:  touch
descr: executes touch unix command, for creating files (or updating their uptime)
input : filename string
'''
def touch(filename):
	touch_run = executable_runner_function("touch")
	touch_run({filename : ""})

'''
name:  ln
descr: executes ln unix command, for creating links
input : symbolic boolean (default false)
	target string
        link_name string
'''
def ln(target,link_name,symbolic=False):
	ln_run = executable_runner_function("ln")

	options = ""

	if symbolic is True:
		options += "-s "

	options += target + " " + link_name

	ln_run({options : ""})

'''
name: pwd
descr: executes pwd unix command, returning actual directory
'''
def pwd():
	import os
	return os.getcwd()
