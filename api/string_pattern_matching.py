def patterns_are_valid(patterns):
    return isinstance(patterns, list) and len(patterns) > 0


def sample_is_valid(sample):
    return isinstance(sample, str)


def patterns_appear_on_sample(sample):
    def patterns_appear(patterns):
        if patterns_are_valid(patterns) is False:
            raise NameError('You must pass a list of patterns!')
        elif sample_is_valid(sample) is False:
            raise NameError('You must pass a string for sample!')

        present_patterns = filter(lambda pattern: pattern in sample, patterns)
        return present_patterns

    return patterns_appear


if __name__ == '__main__':
    sample = "agcattatggtagctatcag"
    patterns = ['at','ta']

    assert len(patterns_appear_on_sample(sample)(patterns)) > 0

    patterns = ['at','ronaldo']

    assert len(patterns_appear_on_sample(sample)(patterns)) > 0

    patterns = ['diamanete']

    assert len(patterns_appear_on_sample(sample)(patterns)) == 0

    print "Tests passed!"

