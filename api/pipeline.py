def pipeline(*function_list):
    def run_pipeline_on(arguments):
        return reduce(lambda result, function: function(result), function_list, arguments)
    return run_pipeline_on
