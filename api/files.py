# files.py
# Handles operations with files
# ------------------------------------
from os import listdir
from os.path import isfile, isdir, join

'''
name: get_lines_of_file
descr: returns an array of lines of a given file
input: filename (string)
output: array of lines
'''
def get_lines_of_file(filename):
	f = open(filename,"r")
	lines = f.readlines()
	f.close()
	return lines


'''
name: files_from_directory
descr: returns an array of filenames of a given directory
input: directory (string) [must be a *path*]
output: array of filenames (array of string)
'''
def files_from_directory(directory):
	file_list = [f for f in listdir(directory) if isfile(join(directory, f))]
	return file_list

'''
name: directories_from_directory
descr: returns an array of directory names of a given directory
input: directory (string) [must be a *path*]
output: array of directory names (array of string)
'''
def directories_from_directory(directory):
	dir_list = [d for d in listdir(directory) if isdir(join(directory, d))]
	return dir_list


'''
name: write_on_file
descr: writes a content on a file
input: filename (string)
       content (string)
'''
def write_on_file(filename,content):
	with open(filename,"w") as file_:
		file_.write(content)
